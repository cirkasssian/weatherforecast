package com.cirkasssian.weatherforecast.data.sources

import com.cirkasssian.weatherforecast.data.models.ForecastResponseDto
import retrofit2.http.GET
import retrofit2.http.Query

internal interface ForecastService {

    @GET("/data/2.5/forecast")
    suspend fun getForecast(
        @Query("q") cityId: String,
        @Query("appid") appId: String,
    ): ForecastResponseDto
}