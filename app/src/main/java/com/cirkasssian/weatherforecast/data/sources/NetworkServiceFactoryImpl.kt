package com.cirkasssian.weatherforecast.data.sources

import retrofit2.Retrofit
import javax.inject.Inject

internal class NetworkServiceFactoryImpl @Inject constructor(
    private val retrofit: Retrofit,
) : NetworkServiceFactory {

    override fun <T> createService(api: Class<T>): T =
        retrofit.create(api)
}