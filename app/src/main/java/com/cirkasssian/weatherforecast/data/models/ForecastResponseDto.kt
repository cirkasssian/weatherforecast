package com.cirkasssian.weatherforecast.data.models

import com.google.gson.annotations.SerializedName

internal data class ForecastResponseDto(
    @SerializedName("city")
    val city: CityDto,
    @SerializedName("cnt")
    val cnt: Int,
    @SerializedName("cod")
    val cod: String,
    @SerializedName("list")
    val periods: List<PeriodDto>,
    @SerializedName("message")
    val message: Int,
)

internal data class CityDto(
    @SerializedName("coord")
    val coord: CoordDto,
    @SerializedName("country")
    val country: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("population")
    val population: Int,
    @SerializedName("sunrise")
    val sunrise: Int,
    @SerializedName("sunset")
    val sunset: Int,
    @SerializedName("timezone")
    val timezone: Int,
)

internal data class PeriodDto(
    @SerializedName("clouds")
    val clouds: CloudsDto,
    @SerializedName("dt")
    val dt: Long,
    @SerializedName("dt_txt")
    val dtTxt: String,
    @SerializedName("main")
    val main: MainDto,
    @SerializedName("pop")
    val pop: Double,
    @SerializedName("rain")
    val rain: RainDto,
    @SerializedName("sys")
    val sys: SysDto,
    @SerializedName("visibility")
    val visibility: Int,
    @SerializedName("weather")
    val weather: List<WeatherDto>,
    @SerializedName("wind")
    val wind: WindDto,
)

internal data class CoordDto(
    @SerializedName("lat")
    val lat: Double,
    @SerializedName("lon")
    val lon: Double,
)

internal data class CloudsDto(
    @SerializedName("all")
    val all: Int,
)

internal data class MainDto(
    @SerializedName("feels_like")
    val feelsLike: Double,
    @SerializedName("grnd_level")
    val grndLevel: Int,
    @SerializedName("humidity")
    val humidity: Int,
    @SerializedName("pressure")
    val pressure: Int,
    @SerializedName("sea_level")
    val seaLevel: Int,
    @SerializedName("temp")
    val temp: Double,
    @SerializedName("temp_kf")
    val tempKf: Double,
    @SerializedName("temp_max")
    val tempMax: Double,
    @SerializedName("temp_min")
    val tempMin: Double,
)

internal data class RainDto(
    @SerializedName("3h")
    val h: Double,
)

internal data class SysDto(
    @SerializedName("pod")
    val pod: String,
)

internal data class WeatherDto(
    @SerializedName("description")
    val description: String,
    @SerializedName("icon")
    val icon: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("main")
    val main: String,
)

internal data class WindDto(
    @SerializedName("deg")
    val deg: Int,
    @SerializedName("gust")
    val gust: Double,
    @SerializedName("speed")
    val speed: Double,
)