package com.cirkasssian.weatherforecast.data.converters

import com.cirkasssian.weatherforecast.data.models.ForecastResponseDto
import com.cirkasssian.weatherforecast.domain.models.DailyForecastEntity
import com.cirkasssian.weatherforecast.domain.models.ForecastEntity
import com.cirkasssian.weatherforecast.domain.models.PeriodEntity
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*


internal fun ForecastResponseDto.toEntity() = with(this) {
    ForecastEntity(
        cityName = city.name,
        days = periods
            .map { period ->
                (period.dt * 1000).toForecastDate() to period
            }
            .groupBy { (date, _) ->
                "${date.day} ${date.month}"
            }
            .map { (date, periods) ->
                DailyForecastEntity(
                    date = date,
                    periods = periods.map { (date, period) ->
                        PeriodEntity(
                            time = date.time,
                            temp = period.main.temp.kelvinToCelsius(),
                            clouds = "${period.clouds.all}%",
                            weather = period.weather.firstOrNull()?.description,
                        )
                    }
                )
            },
    )
}

private data class ForecastDate(
    val day: String,
    val month: String,
    val time: String,
)

private fun Long.toForecastDate() : ForecastDate =
    Date(this).let { date ->
        ForecastDate(
            day = date.parseTo("d"),
            month = date.parseTo("MMMM"),
            time = date.parseTo("hh:mm"),
        )
    }

private fun Date.parseTo(format: String): String {
    val dateFormat = SimpleDateFormat(format, Locale("ru"))
    return dateFormat.format(this)
}

private fun Double.kelvinToCelsius(): String {
    val temp = DecimalFormat("#0.0")
        .format(this - 273.15)
        .replace(",", ".")
    return if (temp.toDouble() > 0) "+$temp" else temp
}