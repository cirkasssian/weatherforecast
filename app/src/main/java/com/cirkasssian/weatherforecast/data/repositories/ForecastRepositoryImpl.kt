package com.cirkasssian.weatherforecast.data.repositories

import com.cirkasssian.weatherforecast.APP_ID
import com.cirkasssian.weatherforecast.data.converters.toEntity
import com.cirkasssian.weatherforecast.data.sources.ForecastService
import com.cirkasssian.weatherforecast.domain.models.ForecastEntity
import com.cirkasssian.weatherforecast.domain.contracts.ForecastRepository
import javax.inject.Inject

internal class ForecastRepositoryImpl @Inject constructor(
    private val service: ForecastService,
) : ForecastRepository {

    override suspend fun getForecast(
        cityId: String,
    ): ForecastEntity = service.getForecast(
        appId = APP_ID,
        cityId = cityId,
    ).toEntity()
}