package com.cirkasssian.weatherforecast.data.sources

internal interface NetworkServiceFactory {

    fun <T> createService(api: Class<T>): T
}