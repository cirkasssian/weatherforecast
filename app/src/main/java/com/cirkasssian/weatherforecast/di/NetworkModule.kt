package com.cirkasssian.weatherforecast.di

import android.content.Context
import android.util.Log
import com.cirkasssian.weatherforecast.BASE_URL
import com.cirkasssian.weatherforecast.data.sources.NetworkServiceFactory
import com.cirkasssian.weatherforecast.data.sources.NetworkServiceFactoryImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@Module
internal interface NetworkModule {

    @Binds
    @ScopeKey(ForecastComponent::class)
    fun NetworkServiceFactoryImpl.bindNetworkService(): NetworkServiceFactory

    companion object {

        private const val DEFAULT_TIMEOUT = 30L

        @Provides
        @ScopeKey(ForecastComponent::class)
        fun provideCache(
            context: Context,
        ): Cache {
            val cacheSize = 10 * 1024 * 1024
            return Cache(context.cacheDir, cacheSize.toLong())
        }

        @Provides
        @ScopeKey(ForecastComponent::class)
        fun provideOkHttpClient(
            cache: Cache,
        ): OkHttpClient = OkHttpClient.Builder()
            .cache(cache)
            .connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
            .addNetworkInterceptor(
                HttpLoggingInterceptor(::log).apply {
                    level = HttpLoggingInterceptor.Level.BODY
                }
            )
            .addNetworkInterceptor(CacheInterceptor())
            .build()

        @Provides
        @ScopeKey(ForecastComponent::class)
        fun provideMoshiConverterFactory(): GsonConverterFactory =
            GsonConverterFactory.create()

        @Provides
        @ScopeKey(ForecastComponent::class)
        fun provideRetrofit(
            client: OkHttpClient,
            converterFactory: GsonConverterFactory,
        ): Retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(converterFactory)
            .build()

        private fun log(message: String) = Log.d("NetworkTag", message)
    }
}

private class CacheInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val response: Response = chain.proceed(chain.request())
        val cacheControl = CacheControl.Builder()
            .maxAge(10, TimeUnit.DAYS)
            .build()
        return response.newBuilder()
            .header("Cache-Control", cacheControl.toString())
            .build()
    }
}