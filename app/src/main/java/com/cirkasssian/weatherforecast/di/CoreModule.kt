package com.cirkasssian.weatherforecast.di

import android.content.Context
import dagger.Module
import dagger.Provides

@Module
internal class CoreModule(
    private val context: Context,
) {

    @Provides
    @ScopeKey(ForecastComponent::class)
    fun provideApplicationContext(): Context = context
}