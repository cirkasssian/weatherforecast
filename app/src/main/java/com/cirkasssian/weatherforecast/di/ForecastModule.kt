package com.cirkasssian.weatherforecast.di

import com.cirkasssian.weatherforecast.data.repositories.ForecastRepositoryImpl
import com.cirkasssian.weatherforecast.data.sources.ForecastService
import com.cirkasssian.weatherforecast.data.sources.NetworkServiceFactory
import com.cirkasssian.weatherforecast.domain.contracts.ForecastRepository
import com.cirkasssian.weatherforecast.domain.contracts.ForecastUseCase
import com.cirkasssian.weatherforecast.domain.usecases.ForecastUseCaseImpl
import com.cirkasssian.weatherforecast.navigation.NavigationManager
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
internal interface ForecastModule {

    @Binds
    @ScopeKey(ForecastComponent::class)
    fun ForecastUseCaseImpl.bindForecastUseCase(): ForecastUseCase

    companion object {

        @Provides
        @ScopeKey(ForecastComponent::class)
        fun provideNavigationManager() = NavigationManager()

        @Provides
        @ScopeKey(ForecastComponent::class)
        fun provideForecastRepository(factory: NetworkServiceFactory): ForecastRepository =
            ForecastRepositoryImpl(factory.createService(ForecastService::class.java))
    }
}