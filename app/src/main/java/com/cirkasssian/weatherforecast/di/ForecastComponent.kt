package com.cirkasssian.weatherforecast.di

import com.cirkasssian.weatherforecast.domain.contracts.ForecastUseCase
import com.cirkasssian.weatherforecast.navigation.NavigationManager
import com.cirkasssian.weatherforecast.presentation.forecast.ForecastViewModel
import com.cirkasssian.weatherforecast.presentation.main.MainViewModel
import dagger.Component

@Component(
    modules = [
        CoreModule::class,
        NetworkModule::class,
        ForecastModule::class,
    ]
)
@ScopeKey(ForecastComponent::class)
internal interface ForecastComponent {

    val navigationManager: NavigationManager

    @ScopeKey(ForecastComponent::class)
    val forecastUseCase: ForecastUseCase

    val mainViewModelFactory: MainViewModel.Factory

    val forecastViewModelFactory: ForecastViewModel.Factory
}

