package com.cirkasssian.weatherforecast.di

import kotlin.reflect.KClass

@javax.inject.Scope
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
internal annotation class ScopeKey(val value: KClass<*>)