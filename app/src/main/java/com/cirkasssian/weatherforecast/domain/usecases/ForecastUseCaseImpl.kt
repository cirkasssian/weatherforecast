package com.cirkasssian.weatherforecast.domain.usecases

import com.cirkasssian.weatherforecast.domain.contracts.ForecastRepository
import com.cirkasssian.weatherforecast.domain.contracts.ForecastUseCase
import com.cirkasssian.weatherforecast.domain.models.ForecastEntity
import javax.inject.Inject

internal class ForecastUseCaseImpl @Inject constructor(
    private val repository: ForecastRepository,
) : ForecastUseCase {

    override suspend fun getForecast(
        cityId: String,
    ): ForecastEntity = repository.getForecast(cityId)
}