package com.cirkasssian.weatherforecast.domain.contracts

import com.cirkasssian.weatherforecast.domain.models.ForecastEntity

internal interface ForecastRepository {

    suspend fun getForecast(
        cityId: String,
    ): ForecastEntity
}