package com.cirkasssian.weatherforecast.domain.contracts

import com.cirkasssian.weatherforecast.domain.models.ForecastEntity

internal interface ForecastUseCase {

    suspend fun getForecast(
        cityId: String,
    ): ForecastEntity
}