package com.cirkasssian.weatherforecast.domain.models

internal data class ForecastEntity(
    val cityName: String,
    val days: List<DailyForecastEntity>,
)

internal data class DailyForecastEntity(
    val date: String,
    val periods: List<PeriodEntity>,
)

internal data class PeriodEntity(
    val time: String,
    val temp: String,
    val clouds: String,
    val weather: String?,
)