package com.cirkasssian.weatherforecast.presentation.base

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cirkasssian.weatherforecast.navigation.NavigationManager
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

internal abstract class BaseViewModel<State : ViewState, Action : ViewAction>(
    private val navigationManager: NavigationManager,
) : ViewModel(), ViewActionListener {

    val state: StateFlow<State> get() = _state
    val effect: Flow<ViewSideEffect> get() = _effect.receiveAsFlow()
    private val initialState: State by lazy { setInitialState() }
    private val _state: MutableStateFlow<State> = MutableStateFlow(initialState)
    private val _action: MutableSharedFlow<ViewAction> = MutableSharedFlow()
    private val _effect: Channel<ViewSideEffect> = Channel()

    init {
        subscribeToEvents()
    }

    override fun onAction(event: ViewAction) {
        viewModelScope.launch(Default) { _action.emit(event) }
    }

    protected fun setEffect(
        builder: () -> ViewSideEffect,
    ) {
        viewModelScope.launch {
            _effect.send(builder())
        }
    }

    abstract fun setInitialState(): State

    open fun handleActions(action: Action) = Unit

    protected fun navigate(directionBuilder: () -> String) =
        directionBuilder().let(navigationManager::navigate)

    protected fun exit() = navigationManager.finish()

    protected fun back() = navigationManager.back()

    protected fun setState(reducer: State.() -> State) {
        val newState = state.value.reducer()
        _state.value = newState
    }

    private fun subscribeToEvents() {
        viewModelScope.launch(Default) {
            _action.collect(::safeHandleActions)
        }
    }

    private fun safeHandleActions(event: ViewAction) = try {
        @Suppress("UNCHECKED_CAST")
        (event as? Action)?.let(::handleActions)
    } catch (e: Throwable) {
        e.printStackTrace()
    }
}
