package com.cirkasssian.weatherforecast.presentation.forecast

import androidx.lifecycle.viewModelScope
import com.cirkasssian.weatherforecast.domain.contracts.ForecastUseCase
import com.cirkasssian.weatherforecast.navigation.NavigationManager
import com.cirkasssian.weatherforecast.presentation.base.BaseViewModel
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.launch

internal class ForecastViewModel @AssistedInject constructor(
    private val forecastUseCase: ForecastUseCase,
    navigationManager: NavigationManager,
) : BaseViewModel<ForecastState, ForecastViewAction>(navigationManager) {

    override fun setInitialState(): ForecastState = ForecastState(isLoading = true)

    fun loadForecast(cityId: String) {
        viewModelScope.launch {
            runCatching { forecastUseCase.getForecast(cityId) }
                .onSuccess { forecast ->
                    setState {
                        copy(
                            isLoading = false,
                            model = forecast,
                        )
                    }
                }
                .onFailure {
                    setState {
                        copy(
                            isLoading = false,
                            isError = true,
                        )
                    }
                }
        }
    }

    override fun handleActions(action: ForecastViewAction) {
        when(action) {
            is ForecastViewAction.Back -> {
                setState {
                    copy(isError = false)
                }
                back()
            }
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(): ForecastViewModel
    }
}