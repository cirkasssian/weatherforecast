package com.cirkasssian.weatherforecast.presentation.base

internal interface ViewSideEffect

internal data class ShowToast(
    val text: String,
) : ViewSideEffect

internal data class OpenLink(
    val url: String,
) : ViewSideEffect