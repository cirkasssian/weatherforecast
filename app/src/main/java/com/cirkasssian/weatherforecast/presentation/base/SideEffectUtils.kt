package com.cirkasssian.weatherforecast.presentation.base

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import kotlinx.coroutines.flow.Flow

@Composable
internal fun <T> Flow<T>?.subscribeToSideEffects(
    key: Any? = Unit,
    callback: (@Composable (T) -> Unit),
) = this?.let { flow ->
    val state: MutableState<T?> = remember { mutableStateOf(null) }
    state.value?.let { safeState ->
        callback.invoke(safeState)
        state.value = null
    }
    LaunchedEffect(key) {
        flow.collect { effect ->
            state.value = effect
        }
    }
}