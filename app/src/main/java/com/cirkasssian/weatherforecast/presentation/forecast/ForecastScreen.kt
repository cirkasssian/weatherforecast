package com.cirkasssian.weatherforecast.presentation.forecast

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.PathEffect
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.ExperimentalUnitApi
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.compose.ui.unit.dp
import com.cirkasssian.weatherforecast.domain.models.DailyForecastEntity
import com.cirkasssian.weatherforecast.domain.models.ForecastEntity
import com.cirkasssian.weatherforecast.domain.models.PeriodEntity
import com.cirkasssian.weatherforecast.presentation.base.CustomButton
import com.cirkasssian.weatherforecast.presentation.base.CustomDialog
import com.cirkasssian.weatherforecast.presentation.base.ViewAction
import com.cirkasssian.weatherforecast.presentation.base.ViewState

@Composable
internal fun ForecastScreen(
    cityId: String?,
    viewModel: ForecastViewModel,
) {
    val screenState by viewModel.state.collectAsState()
    LaunchedEffect(Unit) {
        viewModel.loadForecast(cityId.orEmpty())
    }
    when {
        screenState.isError -> ErrorDialog {
            viewModel.onAction(ForecastViewAction.Back)
        }
        screenState.isLoading -> Loading()
        else -> Content(state = screenState)
    }
}

@OptIn(ExperimentalUnitApi::class)
@Composable
private fun Content(
    state: ForecastState,
) {
    val lazyListState = rememberLazyListState()
    LazyColumn(
        modifier = Modifier.fillMaxSize(),
        contentPadding = PaddingValues(16.dp),
        state = lazyListState,
    ) {
        item {
            Text(
                modifier = Modifier.fillMaxWidth(),
                text = state.model?.cityName.orEmpty(),
                textAlign = TextAlign.Center,
                fontSize = TextUnit(28f, TextUnitType.Sp),
            )
        }
        state.model?.days?.let { days ->
            items(days) { DailyCard(it) }
        }
    }
}

@Composable
private fun DailyCard(model: DailyForecastEntity) {
    Card(
        shape = RoundedCornerShape(16.dp),
        modifier = Modifier.padding(vertical = 8.dp),
        elevation = 8.dp,
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp),
        ) {
            Text(
                modifier = Modifier.fillMaxWidth(),
                text = model.date,
            )
            model.periods.forEach { PeriodCard(it) }
        }
    }
}

private val stroke = Stroke(width = 2f,
    pathEffect = PathEffect.dashPathEffect(floatArrayOf(10f, 10f), 0f)
)

@OptIn(ExperimentalUnitApi::class)
@Composable
private fun PeriodCard(model: PeriodEntity) {
    Column(
        modifier = Modifier.fillMaxWidth()
            .padding(vertical = 8.dp)
            .drawBehind {
                drawRoundRect(
                    color = Color.Red,
                    style = stroke,
                    cornerRadius = CornerRadius(20f, 20f)
                )
            }
            .padding(8.dp),
    ) {
        Text(
            modifier = Modifier.fillMaxWidth(),
            text = model.time,
        )
        model.weather?.let { weather ->
            Text(
                modifier = Modifier.fillMaxWidth(),
                text = weather,
                fontSize = TextUnit(12f, TextUnitType.Sp),
            )
        }
        Text(
            modifier = Modifier.fillMaxWidth(),
            text = "Temp: ${model.temp}",
            fontSize = TextUnit(12f, TextUnitType.Sp),
        )
        Text(
            modifier = Modifier.fillMaxWidth(),
            text = "Clouds: ${model.clouds}",
            fontSize = TextUnit(12f, TextUnitType.Sp),
        )
    }
}

@Composable
private fun Loading() {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        CircularProgressIndicator(
            modifier = Modifier
                .padding(top = 32.dp)
                .size(38.dp),
        )
    }
}

@Composable
private fun ErrorDialog(onDismiss: () -> Unit) {
    CustomDialog(
        onDismiss = onDismiss,
        primaryButton = CustomButton(
            "Понятно",
            onDismiss,
        ),
    ) {
        Text(
            text = "Произошла ошибка",
            textAlign = TextAlign.Center,
        )
    }
}

internal data class ForecastState(
    override val isLoading: Boolean = false,
    override val isError: Boolean = false,
    val model: ForecastEntity? = null,
): ViewState()

internal sealed interface ForecastViewAction : ViewAction {

    object Back : ForecastViewAction
}