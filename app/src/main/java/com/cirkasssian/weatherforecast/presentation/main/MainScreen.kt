package com.cirkasssian.weatherforecast.presentation.main

import android.widget.Toast
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.ExperimentalUnitApi
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.compose.ui.unit.dp
import com.cirkasssian.weatherforecast.presentation.base.CustomButton
import com.cirkasssian.weatherforecast.presentation.base.CustomDialog
import com.cirkasssian.weatherforecast.presentation.base.OpenLink
import com.cirkasssian.weatherforecast.presentation.base.ShowToast
import com.cirkasssian.weatherforecast.presentation.base.ViewAction
import com.cirkasssian.weatherforecast.presentation.base.ViewSideEffect
import com.cirkasssian.weatherforecast.presentation.base.ViewState
import com.cirkasssian.weatherforecast.presentation.base.subscribeToSideEffects

@OptIn(ExperimentalUnitApi::class)
@Composable
internal fun MainScreen(
    viewModel: MainViewModel,
) {
    val screenState by viewModel.state.collectAsState()
    viewModel.effect.subscribeToSideEffects { ProcessSideEffect(it) }
    var showFinishDialog by remember { mutableStateOf(false) }
    if (showFinishDialog) NoCitySelectedDialog { showFinishDialog = false }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
    ) {
        Text(
            modifier = Modifier.fillMaxWidth(),
            text = screenState.title.orEmpty(),
            textAlign = TextAlign.Center,
            fontSize = TextUnit(28f, TextUnitType.Sp)
        )
        var cityId by rememberSaveable { mutableStateOf("") }
        TextField(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 16.dp),
            value = cityId,
            onValueChange = { cityId = it },
            label = { Text(screenState.hint.orEmpty()) }
        )
        Button(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 8.dp),
            onClick = {
                val cityIdTrimmed = cityId.trim()
                if (cityIdTrimmed.isNotEmpty()) {
                    viewModel.onAction(MainViewAction.OpenForecastClick(cityIdTrimmed))
                } else {
                    showFinishDialog = true
                }
            },
        ) {
            Text(text = screenState.buttonText.orEmpty())
        }
        Spacer(modifier = Modifier.height(16.dp))
        Button(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 8.dp),
            onClick = {
                viewModel.onAction(MainViewAction.ShowToastClick("Тост"))
            },
        ) {
            Text(text = "Показать тост")
        }
    }
}

@Composable
private fun ProcessSideEffect(effect: ViewSideEffect) {
    when (effect) {
        is ShowToast -> Toast.makeText(LocalContext.current, effect.text, Toast.LENGTH_SHORT).show()
        is OpenLink -> LocalUriHandler.current.openUri(effect.url)
    }
}

@Composable
private fun NoCitySelectedDialog(onDismiss: () -> Unit) {
    CustomDialog(
        onDismiss = onDismiss,
        primaryButton = CustomButton(
            "Понятно",
            onDismiss,
        ),
    ) {
        Text(
            text = "Вы не ввели код города",
            textAlign = TextAlign.Center,
        )
    }
}

internal data class MainState(
    override val isLoading: Boolean = false,
    override val isError: Boolean = false,
    val title: String? = null,
    val hint: String? = null,
    val buttonText: String? = null,
): ViewState()

internal sealed interface MainViewAction : ViewAction {

    data class OpenForecastClick(
        val cityId: String,
    ) : MainViewAction

    data class ShowToastClick(
        val text: String,
    ) : MainViewAction
}