package com.cirkasssian.weatherforecast.presentation.base

internal abstract class ViewState(
    open val isLoading: Boolean = false,
    open val isError: Boolean = false,
)