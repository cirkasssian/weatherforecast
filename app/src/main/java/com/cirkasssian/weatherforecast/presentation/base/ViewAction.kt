package com.cirkasssian.weatherforecast.presentation.base

internal interface ViewActionListener {

    fun onAction(event: ViewAction)
}

internal interface ViewAction