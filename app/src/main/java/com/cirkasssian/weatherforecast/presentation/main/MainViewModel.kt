package com.cirkasssian.weatherforecast.presentation.main

import com.cirkasssian.weatherforecast.navigation.ForecastNavEntry
import com.cirkasssian.weatherforecast.navigation.NavigationManager
import com.cirkasssian.weatherforecast.presentation.base.BaseViewModel
import com.cirkasssian.weatherforecast.presentation.base.ShowToast
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject

internal class MainViewModel @AssistedInject constructor(
    navigationManager: NavigationManager,
) : BaseViewModel<MainState, MainViewAction>(navigationManager) {

    override fun setInitialState(): MainState = MainState(
        isLoading = false,
        title = "Прогноз погоды",
        hint = "Введите код города",
        buttonText = "Найти",
    )

    override fun handleActions(action: MainViewAction) {
        when (action) {
            is MainViewAction.OpenForecastClick -> navigate {
                ForecastNavEntry.Forecast.getNavigationRoute(action.cityId)
            }
            is MainViewAction.ShowToastClick -> setEffect {
                ShowToast(action.text)
            }
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(): MainViewModel
    }
}