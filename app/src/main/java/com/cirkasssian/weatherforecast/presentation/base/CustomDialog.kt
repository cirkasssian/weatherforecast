package com.cirkasssian.weatherforecast.presentation.base

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties

@Composable
internal fun CustomDialog(
    onDismiss: (() -> Unit)? = null,
    primaryButton: CustomButton? = null,
    secondaryButton: CustomButton? = null,
    content: (@Composable ColumnScope.() -> Unit)? = null,
) {
    Dialog(
        properties = DialogProperties(
            dismissOnBackPress = true,
        ),
        onDismissRequest = { onDismiss?.invoke() }) {
        Card(
            shape = RoundedCornerShape(16.dp),
            modifier = Modifier.padding(16.dp),
            elevation = 8.dp,
        ) {
            Column(
                Modifier.fillMaxWidth()
                    .padding(16.dp),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                content?.invoke(this)
                if (primaryButton != null || secondaryButton != null) {
                    Row(Modifier.padding(top = 16.dp)) {
                        secondaryButton?.let { secondary ->
                            OutlinedButton(
                                onClick = secondary.onClick,
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .weight(1F)
                            ) {
                                Text(text = secondary.name)
                            }
                        }
                        if (primaryButton != null && secondaryButton != null) {
                            Spacer(modifier = Modifier.width(16.dp))
                        }
                        primaryButton?.let { primary ->
                            Button(
                                onClick = primary.onClick,
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .weight(1F)
                            ) {
                                Text(text = primary.name)
                            }
                        }
                    }
                }
            }
        }
    }
}

internal data class CustomButton(
    val name: String,
    val onClick: () -> Unit,
)