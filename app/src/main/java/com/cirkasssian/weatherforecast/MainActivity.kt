package com.cirkasssian.weatherforecast

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.navigation.compose.rememberNavController
import com.cirkasssian.weatherforecast.di.CoreModule
import com.cirkasssian.weatherforecast.di.DaggerForecastComponent
import com.cirkasssian.weatherforecast.navigation.ForecastNavGraph
import com.cirkasssian.weatherforecast.navigation.NavigationAction
import com.cirkasssian.weatherforecast.presentation.base.theme.WeatherForecastTheme

internal class MainActivity : ComponentActivity() {

    private val component by lazy {
        DaggerForecastComponent
            .builder()
            .coreModule(CoreModule(this))
            .build()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            WeatherForecastTheme {
                val navController = rememberNavController()
                LaunchedEffect(Unit) {
                    component.navigationManager.commands.collect { action ->
                        when (action.action) {
                            NavigationAction.NAVIGATE -> action.route?.let(navController::navigate)
                            NavigationAction.BACK -> navController.navigateUp()
                            NavigationAction.FINISH -> onBackPressed()
                        }
                    }
                }
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    ForecastNavGraph(
                        navController = navController,
                        component,
                    )
                }
            }
        }
    }
}