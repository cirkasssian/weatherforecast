package com.cirkasssian.weatherforecast.navigation

import androidx.navigation.NavType
import androidx.navigation.navArgument

internal sealed class ForecastNavEntry(
    override val route: String,
) : BaseNavEntry {

    object Main : ForecastNavEntry("MAIN")

    object Forecast : ForecastNavEntry("FORECAST") {

        const val CITY_ID = "CITY_ID"

        override val arguments = listOf(
            navArgument(CITY_ID) {
                type = NavType.StringType
            },
        )
    }
}
