package com.cirkasssian.weatherforecast.navigation

import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.staticCompositionLocalOf
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.NavHostController
import com.cirkasssian.weatherforecast.di.ForecastComponent
import com.cirkasssian.weatherforecast.navigation.ForecastNavEntry.Forecast.CITY_ID
import com.cirkasssian.weatherforecast.presentation.base.daggerViewModel
import com.cirkasssian.weatherforecast.presentation.forecast.ForecastScreen
import com.cirkasssian.weatherforecast.presentation.main.MainScreen


@Composable
internal fun ForecastNavGraph(
    navController: NavHostController,
    component: ForecastComponent,
) {
    CompositionLocalProvider(LocalNavController provides navController) {
        NavHost(
            navController = navController,
            startDestination = ForecastNavEntry.Main.getComposableRoute(),
        ) {
            composable(route = ForecastNavEntry.Main.getComposableRoute()) {
                MainScreen(
                    viewModel = daggerViewModel {
                        component.mainViewModelFactory.create()
                    },
                )
            }
            composable(
                route = ForecastNavEntry.Forecast.getComposableRoute(),
                arguments = ForecastNavEntry.Forecast.arguments,
            ) { navEntry ->
                ForecastScreen(
                    cityId = navEntry.arguments?.getString(CITY_ID),
                    viewModel = daggerViewModel {
                        component.forecastViewModelFactory.create()
                    },
                )
            }
        }
    }
}

internal val LocalNavController = staticCompositionLocalOf<NavHostController> {
    error("CompositionLocal LocalNavController not present")
}