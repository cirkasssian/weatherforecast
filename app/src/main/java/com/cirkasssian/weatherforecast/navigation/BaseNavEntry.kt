package com.cirkasssian.weatherforecast.navigation

import androidx.navigation.NamedNavArgument

internal interface BaseNavEntry {

    val route: String

    val arguments: List<NamedNavArgument>
        get() = emptyList()

    fun getComposableRoute() = if (arguments.isNotEmpty()) buildString {
        append(route)
        arguments.forEach { arg ->
            append("/{${arg.name}}")
        }
    } else route

    fun getNavigationRoute(vararg args: Any?) = if (args.isNotEmpty()) buildString {
        append(route)
        args.forEach { arg ->
            append("/$arg")
        }
    } else route
}