package com.cirkasssian.weatherforecast.navigation

import androidx.navigation.NamedNavArgument
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.receiveAsFlow

internal enum class NavigationAction {
    NAVIGATE,
    BACK,
    FINISH,
}

internal data class  NavigationCommand(
    val action: NavigationAction,
    val route: String? = null,
    val arguments: List<NamedNavArgument> = emptyList(),
)

internal class NavigationManager {
    val commands: Flow<NavigationCommand>
        get() = _commands.receiveAsFlow()
    private var _commands = Channel<NavigationCommand>()

    fun navigate(route: String) {
        _commands.trySend(NavigationCommand(NavigationAction.NAVIGATE, route))
    }

    fun finish() {
        _commands.trySend(NavigationCommand(NavigationAction.FINISH))
    }

    fun back() {
        _commands.trySend(NavigationCommand(NavigationAction.BACK))
    }
}